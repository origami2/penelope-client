var assert = require('assert');
var PenelopeClientFactory = require('..');

describe('PenelopeClientFactory', function () {
  it('exports a function', function () {
    assert.equal('function', typeof(PenelopeClientFactory));
  });
  
  it('requires a resolution function', function () {
    assert.throws(
      function () {
        new PenelopeClientFactory();
      },
      /resolution function is required/
    );
  });
});